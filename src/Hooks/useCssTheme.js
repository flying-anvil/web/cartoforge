// @flow

import type {ThemeType} from "../Data/Theme/Themes";
import {useLayoutEffect} from "react";

const setCssVar = (name: string, value: any) => {
  document.documentElement.style.setProperty(`--${name}`, value);
}

export default function useCssTheme(theme: ThemeType) {
  useLayoutEffect(() => {
    setCssVar('color-background', theme.map.background);
    setCssVar('color-text', theme.map.background);
  }, [theme])
}
