// @flow

import {useEffect, useRef} from "react";

export type DeltaType = {
  x: number,
  y: number,
}

export default function useDrag(onDrag: Function<DeltaType>) {
  const isMovingRef = useRef(false);

  const startDrag = () => {
    // console.log('Start dragging');

    isMovingRef.current = true;
  }

  const doMove = (event) => {
    if (isMovingRef.current === true) {
      if (event.touches) {
        return;
      }

      const delta = {
        x: event.movementX,
        y: event.movementY,
      };

      onDrag && onDrag(delta);
    }
  }

  const stopDrag = () => {
    if (!isMovingRef.current) return;

    // console.log('Stop dragging');

    isMovingRef.current = false;
  }

  useEffect(() => {
    document.body.addEventListener('mouseup', stopDrag);
    document.body.addEventListener('mousemove', doMove);

    document.body.addEventListener('touchcancel', stopDrag);
    document.body.addEventListener('touchmove', doMove);

    return () => {
      // console.log('cleaning up');

      document.body.removeEventListener('mouseup', stopDrag);
      document.body.removeEventListener('mousemove', doMove);

      document.body.removeEventListener('touchcancel', stopDrag);
      document.body.removeEventListener('touchmove', doMove);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [onDrag])

  return [startDrag];
}
