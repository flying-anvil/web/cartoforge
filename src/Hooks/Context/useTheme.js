// @flow

import {useContext} from "react";
import ThemeContext from "../../Context/ThemeContext";
import type {ThemeType} from "../../Data/Theme/Themes";

export default function useTheme(): ThemeType {
  return useContext(ThemeContext);
}
