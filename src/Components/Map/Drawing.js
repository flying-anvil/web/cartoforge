// @flow

import type {WH, XY} from "../../Types/XY";
import type {ThemeType} from "../../Data/Theme/Themes";
import type {MapSettingsType} from "./mapSettingsReducer";

function drawBackground(ctx: CanvasRenderingContext2D, canvasSize: WH, theme: ThemeType) {
  ctx.clearRect(0, 0, canvasSize.width, canvasSize.height);

  // ctx.fillStyle = theme.map.background;
  // ctx.fillRect(0, 0, canvasSize.width, canvasSize.height);
}

function drawGrid(ctx: CanvasRenderingContext2D, theme: ThemeType, center: XY, offset: XY, settings: MapSettingsType) {
  if (!settings.showGrid) return;


}

function drawOrigin(ctx: CanvasRenderingContext2D, theme: ThemeType, center: XY, offset: XY, settings: MapSettingsType) {
  if (!settings.showOrigin) return;

  ctx.fillStyle = theme.ui.text;
  ctx.beginPath();
  ctx.arc(center.x + offset.x, center.y - offset.y, 5, 0, 2 * Math.PI);
  ctx.fillStyle = theme.map.origin.fill;
  ctx.fill();
  ctx.strokeStyle = theme.map.origin.stroke;
  ctx.stroke();
}

type DrawArguments = {
  ctx: CanvasRenderingContext2D,
  canvasSize: WH,
  theme: ThemeType,
  settings: MapSettingsType,
  offset: XY,
}
export default function drawMap(
  {
    ctx,
    canvasSize,
    theme,
    settings,
    offset,
  }: DrawArguments
) {
  const center = {
    x: canvasSize.width * 0.5,
    y: canvasSize.height * 0.5,
  }

  const startTime = performance.now();

  drawBackground(ctx, canvasSize, theme);
  drawGrid(ctx, theme, center, offset, settings);
  drawOrigin(ctx, theme, center, offset, settings)

  const endTime = performance.now();
  const duration = endTime - startTime;
  return [duration];
}
