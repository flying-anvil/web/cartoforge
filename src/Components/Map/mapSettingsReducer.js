// @flow

type DataType = {
  name: string,
  value: any,
}

export type SettingOperationType = 'set' | 'toggle';

type ActionType = {
  operation: SettingOperationType,
  data: DataType,
}

export type MapSettingsType = {
  showOrigin: boolean,
  showGrid: boolean,
}

export const defaultMapSettings: MapSettingsType = {
  showOrigin: true,
  showGrid: true,
}

export default function mapSettingsReducer(state: MapSettingsType, action: ActionType) {
  const {operation, data} = action;

  switch (operation) {
    case 'set':
      return {...state, [data.name]: data.value};
    case 'toggle':
      return {...state, [data.name]: !state[data.name]};
    default:
      throw new Error(`Unknown operation "${operation}"`);
  }
}
