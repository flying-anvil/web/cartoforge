// @flow
import React, {useEffect, useRef} from 'react';
import useWindowSize from "../../Hooks/useWindowSize";
import useTheme from "../../Hooks/Context/useTheme";
import useDrag from "../../Hooks/useDrag";
import type {XY} from "../../Types/XY";
import drawMap from "./Drawing";
import {MapSettingsType} from "./mapSettingsReducer";

type Props = {
  onMapValueChange: Function<string, string, any>,
  offset: XY,
  settings: MapSettingsType,
};

export default function Map(props: Props) {
  const {onMapValueChange, offset, settings} = props;

  const canvasRev = useRef();
  const windowSize = useWindowSize();
  const theme = useTheme();
  const ctxRef = useRef();

  // TODO: Causes a lot of re-registering event-listeners, useCallback does not help
  const [startDrag] = useDrag((delta) => {
    onMapValueChange('offset', 'delta', delta);

    // const [duration] = drawMap({
    //   ctx: ctxRef.current,
    //   canvasSize: windowSize,
    //   theme,
    //   settings,
    //   offset,
    // });
  });

  useEffect(() => {
    const canvas: HTMLCanvasElement = canvasRev.current;

    if (canvas.width !== windowSize.width || canvas.height !== windowSize.height) {
      canvas.width  = windowSize.width;
      canvas.height = windowSize.height;

      console.log(`canvas dimensions: %c${canvas.width}x${canvas.height}`, 'font-size: 20px');
    }

    if (!ctxRef.current) {
      ctxRef.current = canvas.getContext('2d');
    }

    const [duration] = drawMap({
      ctx: ctxRef.current,
      canvasSize: windowSize,
      theme,
      settings,
      offset,
    });

    onMapValueChange('drawTime', 'set', duration);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [windowSize, theme, settings, offset]);

  return (
    <>
      {/*<svg theme={{zIndex: 2}} width="100%" height="10000px" xmlns="http://www.w3.org/2000/svg">*/}
      {/*  <defs>*/}
      {/*    <pattern id="smallGrid" width="8" height="8" patternUnits="userSpaceOnUse">*/}
      {/*      <path d="M 8 0 L 0 0 0 8" fill="none" stroke="gray" strokeWidth="0.5" />*/}
      {/*    </pattern>*/}
      {/*    <pattern id="grid" width="80" height="80" patternUnits="userSpaceOnUse">*/}
      {/*      <rect width="80" height="80" fill="url(#smallGrid)" />*/}
      {/*      <path d="M 80 0 L 0 0 0 80" fill="none" stroke="gray" strokeWidth="1" />*/}
      {/*    </pattern>*/}
      {/*  </defs>*/}

      {/*  <rect width="100%" height="100%" fill="url(#grid)" />*/}
      {/*</svg>*/}

      <canvas onMouseDown={startDrag} id={'map'} ref={canvasRev} style={{
        backgroundImage: 'url(/grid.svg)',
        backgroundPosition: `${offset.x}px ${-offset.y}px`
      }}/>
    </>
  );
}
