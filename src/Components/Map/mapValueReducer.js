// @flow

import type {XY} from "../../Types/XY";

type DataType = {
  name: string,
  value: any,
}

type ActionType = {
  operation: 'set' | 'delta',
  data: DataType,
}

export type MapValuesType = {
  offset?: XY,
  drawTime?: number,
  [string]: any,
}

export const defaultMapValues: MapValuesType = {
  offset: {
    x: 0,
    y: 0,
  }
}

const updateDelta = (state: MapValuesType, name: string, delta) => {
  switch (name) {
    case 'offset':
      const old = state.offset;
      return {
        x: old.x + delta.x,
        y: old.y - delta.y,
      };
    default:
      throw new Error(`Unknown parameter "${name}" for delta operation`);
  }
}

export default function mapValueReducer(state: MapValuesType, action: ActionType) {
  const {operation, data} = action;

  switch (operation) {
    case 'set':
      return {...state, [data.name]: data.value};
    case 'delta':
      return {...state, [data.name]: updateDelta(state, data.name, data.value)};
    default:
      throw new Error(`Unknown operation "${operation}"`);
  }
}
