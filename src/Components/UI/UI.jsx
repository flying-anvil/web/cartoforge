// @flow
import React from 'react';
import {Col, Row} from "react-bootstrap";
import {GiTreasureMap, RiTreasureMapLine} from "react-icons/all";
import type {ThemeType} from "../../Data/Theme/Themes";
import Window from "./Window";
import type {MapValuesType} from "../Map/mapValueReducer";
import Settings from "./Settings";
import type {SettingOperationType} from "../Map/mapSettingsReducer";

type Props = {
  theme: ThemeType,
  setTheme: ThemeType,
  mapValues: MapValuesType,
  onSettingChange: Function<string, SettingOperationType, any>,
};

export default function UI(props: Props) {
  const {theme, setTheme, mapValues, onSettingChange} = props;
  const {offset, drawTime} = mapValues;

  return (
    <Col md={12} style={{zIndex: 1, position: 'absolute'}}>
      <Row>
        <Col>
          <h2>CartoForge <RiTreasureMapLine/> <GiTreasureMap/></h2>
        </Col>
      </Row>
      <Row className={'fixed-bottom m-3'} style={{bottom: '0px'}}>
        <Col col={4}>
          <Window title={'Settings'}>
            <Settings
              theme={theme}
              setTheme={setTheme}
              onSettingChange={onSettingChange}
            />
          </Window>
        </Col>
        <Col md={4}/>
        <Col col={4} className={'text-right'}>
          <Window title={'Debug'}>
            Map Offset: [{-offset.x}, {-offset.y}]
            <br/>
            Draw Time: {(drawTime || 0).toFixed(2)} ms
          </Window>
        </Col>
      </Row>
    </Col>
  );
}
