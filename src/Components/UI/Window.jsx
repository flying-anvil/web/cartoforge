// @flow
import React, {useState} from 'react';
import {Card, Col, Row} from "react-bootstrap";
import useDrag from "../../Hooks/useDrag";
import '../../Style/Window.css'
import type {XY} from "../../Types/XY";
import {BiWindow} from "react-icons/all";

type Props = {
  title: string,
  children: any,
  defaultPosition?: XY,
};

export default function Window(props: Props) {
  const {title, children, defaultPosition} = props;

  const [position, setPosition] = useState(defaultPosition || {x: 0, y: 0});

  const [startDrag] = useDrag((delta) => {
    setPosition((old) => ({
      x: old.x + delta.x,
      y: old.y - delta.y,
    }))
  });

  const resetPosition = () => {
    setPosition(defaultPosition || {x: 0, y: 0});
  }

  return (
    <Card style={{position: "absolute", left: position.x, bottom: position.y}}>
      <Card.Header
        onMouseDown={startDrag}
        onTouchStart={startDrag}
        className={'cursor-move no-select text-left pt-1 pb-1 pl-3 pr-2'}
      >
        <Row className={'pl-0 flex-nowrap'}>
          <Col>
            {title}
          </Col>
          <Col className={'text-right'}>
            <BiWindow title={'Reset position'} className={'cursor-pointer'} onClick={resetPosition}/>
          </Col>
        </Row>
      </Card.Header>
      <Card.Body className={'px-3 pt-2 pb-1'}>
        {children}
      </Card.Body>
    </Card>
  );
}
