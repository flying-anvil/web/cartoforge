// @flow
import React, {useState} from 'react';
import {Form} from "react-bootstrap";
import type {SettingOperationType} from "../Map/mapSettingsReducer";
import type {ThemeType} from "../../Data/Theme/Themes";
import allThemes from "../../Data/Theme/Themes";

type Props = {
  onSettingChange: (string, SettingOperationType, any) => void,
  theme: ThemeType,
  setTheme: ThemeType,
};

export default function Settings(props: Props) {
  const {onSettingChange, theme, setTheme} = props;

  const [selectedThemeKey, setSelectedThemeKey] = useState('defaultDark');

  const onChange = (event) => {
    onSettingChange(event.target.name, 'set', event.target.checked)
  }

  const onThemeChange = (event) => {
    const newName = event.target.value;
    const newTheme = allThemes[newName];

    setSelectedThemeKey(newName);
    setTheme(newTheme);
  }

  return (
    <>
      <Form.Group controlId="settings-show-origin">
        <Form.Check name={'showOrigin'} type={'checkbox'} label={'Show Origin'} onChange={onChange}/>
      </Form.Group>
      <Form.Group controlId="settings-show-grid">
        <Form.Check name={'showGrid'} type={'checkbox'} label={'Show Grid'} onChange={onChange}/>
      </Form.Group>

      <hr/>

      <Form.Group controlId="select-preset">
        <Form.Label>Theme</Form.Label>
        <Form.Control as="select" value={selectedThemeKey} onChange={onThemeChange} custom>
          {Object.keys(allThemes).map((key: ThemeType) =>
            <option value={key} key={key}>
              {allThemes[key].name}
            </option>
          )}
        </Form.Control>
      </Form.Group>
    </>
  );
}
