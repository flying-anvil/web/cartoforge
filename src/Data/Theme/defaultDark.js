// @flow

import type {ThemeType} from "./Themes";

const theme: ThemeType = {
  name: 'Default Dark',
  map: {
    // background: '#060505',
    // background: '#12110F',
    background: '#292A2D',
    origin: {
      fill: '#437c93',
      stroke: '#23434e',
    },
  },
  ui: {
    text: '#CFC5B9',
  }
}

export default theme;
