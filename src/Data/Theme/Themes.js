// @flow

import defaultDark from "./defaultDark";
import testTheme from "./defaultLight";

export type ThemeType = {
  name: string,
  map: {
    background: string, // TODO: Change to Color
    origin: {
      fill: string,
      stroke: string,
    },
  },
  ui: {
    text: string,
  }
}

const allThemes = {
  defaultLight: testTheme,
  defaultDark: defaultDark,
};

export default allThemes;
