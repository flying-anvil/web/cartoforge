// @flow

import type {ThemeType} from "./Themes";

const theme: ThemeType = {
  name: 'Default Light',
  map: {
    // background: '#060505',
    // background: '#12110F',
    background: '#ccc',
    origin: {
      // fill: '#9a5011',
      // stroke: '#3d0202',
      fill: '#437c93',
      stroke: '#23434e',
    },
  },
  ui: {
    text: '#CFC5B9',
  }
}

export default theme;
