// @flow

import {createContext} from "react";
import allThemes from "../Data/Theme/Themes";

const ThemeContext = createContext(allThemes.defaultDark);

export default ThemeContext;
