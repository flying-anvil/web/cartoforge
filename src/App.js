// @flow

import Map from "./Components/Map/Map";
import UI from "./Components/UI/UI";
import {useEffect, useReducer, useState} from "react";
import allThemes from "./Data/Theme/Themes";
import ThemeContext from "./Context/ThemeContext";
import useCssTheme from "./Hooks/useCssTheme";
import './Style/Map.css';
import './Style/Cursor.css';
import mapValueReducer, {defaultMapValues} from "./Components/Map/mapValueReducer";
import mapSettingsReducer, {defaultMapSettings} from "./Components/Map/mapSettingsReducer";
import type {SettingOperationType} from "./Components/Map/mapSettingsReducer";

function App() {
  const [theme, setTheme] = useState(allThemes.defaultDark);
  const [mapValues, dispatchMapValue] = useReducer(mapValueReducer, defaultMapValues);
  const [settings, dispatchSettings] = useReducer(mapSettingsReducer, defaultMapSettings);

  useEffect(() => {
    document.body.style = `background-color: ${theme.map.background}`;
  }, [theme])

  useCssTheme(theme);

  const updateMapValue = (name: string, operation: string, value: any) => {
    dispatchMapValue({
      operation: operation,
      data: {
        name,
        value,
      },
    });
  }

  const updateSetting = (name: string, operation: SettingOperationType, value: any) => {
    dispatchSettings({
      operation: operation,
      data: {
        name,
        value,
      },
    });
  }

  return (
    <>
      <ThemeContext.Provider value={theme}>
        <UI theme={theme} setTheme={setTheme} mapValues={mapValues} onSettingChange={updateSetting}/>
        <Map onMapValueChange={updateMapValue} offset={mapValues.offset} settings={settings}/>
      </ThemeContext.Provider>
    </>
  );
}

export default App;
