// @flow

export type XY = {
  x: number,
  y: number,
}

export type WH = {
  width: number,
  height: number,
}

export type XYZ = {
  x: number,
  y: number,
  z: number,
}
